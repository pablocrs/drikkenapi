<?php

class PostalcodesController extends \BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth.vendor',array('except' => array('index','show')));
        $this->beforeFilter('csrf', array('except' => array('index','show')));
    }

	public function index()
	{
		$data = new Postalcodes;
		$postalcode = Input::get('pc');

		if($postalcode){	
			$data = $data->getByPc($postalcode);
		}
		else{
			$data = $data->all();
		}

		return Response::json(
			$data,
			202
		);
	}


	public function create()
	{
		//
	}


	public function store()
	{
		//
	}


	public function show($user)
	{
        $postalcode = new Postalcodes;

        $validator = Validator::make(
            array(
                'number' => $postalcode->number = Input::get('number'),
                'zone' => $postalcode->zone = Input::get('zone'),
                'user' => $postalcode->user = Auth::user()->email
            ),
            array(
                'number' => 'required',
                'zone' => 'required',
                'user' => 'required|email',
            )
        );

        if( $validator->passes() )
        {
            if( $postalcode->save() )
            {
                return Response::json(
                    $postalcode,
                    201
                );
            }
            else
            {
                return Response::json(
                    'Server error',
                    500
                );
            }
        }
        else
        {
            return Response::json(
                $validator->messages(),
                403
            );
        }
	}


	public function edit($id)
	{
		// TODO: EDIT POSTAL CODE
	}


	public function update($id)
	{
		//
	}


	public function destroy($id)
	{
		// TODO: DESTROY POSTAL CODE
	}

}