<?php

class ZoneController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth.vendor',array('except' => array('index','show')));
        $this->beforeFilter('csrf', array('except' => array('index','show')));
    }

	public function index()
	{
		$zones = new Zones;
		$postalcode = Input::get('pc');
		if($postalcode){
			$data = $zones->getByPc($postalcode);
		}
		else{
			$data = $zones->getByUser(Auth::user()->email);
		}
		return Response::json(
			$data,
			202
		);
	}


	public function create()
	{	
		
	}


	public function store()
	{
        $zone = new Zones;

        $validator = Validator::make(
            array(
                'name' => $zone->name = Input::get('name'),
                'description' => $zone->description = Input::get('description'),
                'url' => $zone->url = Input::get('url'),
                'user' => $zone->user = Auth::user()->email
            ),
            array(
                'name' => 'required',
                'description' => 'required',
                'user' => 'required|email',
            )
        );
        if( $validator->passes() )
        {
            if( $zone->save() )
            {
                return Response::json(
                    $zone,
                    201
                );
            }
            else
            {
                return Response::json(
                    'Server error',
                    500
                );
            }
        }
        else
        {
            return Response::json(
                $validator->messages(),
                403
            );
        }
	}


	public function show($user)
	{
		$zones = new Zones;
		$data = $zones->getByUser($user);
		return Response::json(
			$data,
			202
		);
	}


	public function edit($id)
	{
		return $id;
	}


	public function update($id)
	{
		//
	}


	public function destroy($id)
	{
		$item = Zones::find($id);
		$item->delete();
		return Response::json(
			$item,
			200
		);
	}

}