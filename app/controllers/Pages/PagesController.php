<?php

class PagesController extends BaseController {


	public function index()
	{
        $page = new Pages;

        $lang = Input::get('lang');

        $data = $page->getByName('home',$lang);

        return Response::json($data,200);
	}


	public function create()
	{
		//
	}


	public function store()
	{
		//
	}


	public function show($url)
	{
		$page = new Pages;
        $user = Input::get('user');

        $data = $page->getByUrl($url,$user);

        if ($data != false){
            return Response::json(
                $data,
                202
            );
        }
        else {
            return Response::json(
                '404',
                404
            );
        }
	}


	public function edit($id)
	{
		//
	}


	public function update($id)
	{
		//
	}


	public function destroy($id)
	{
		//
	}

}