<?php

class CartController extends BaseController {


	public function index()
	{
		$cart = array(
			'content' => Cart::content()->toArray() ,
			'total' => Cart::total(),
			'count' => Cart::count()
		);

		return Response::json(
			$cart,
			202
		);
	}

	public function create()
	{
		//
	}

	public function store()
	{
		$item =	array(
			'id' => Input::get('id'),
			'name' => Input::get('name'),
			'qty' => Input::get('qty'),
			'price' => Input::get('price')
		);		

		$validator = Validator::make($item,
			array(
				'id' => 'required',
				'name' => 'required',
				'qty' => 'required',
				'price' => 'required',
			)
		);

		if( $validator->passes() ){
			
			$data = DB::table('products')
				->where('id',$item['id'])
				->where('pvp',$item['price'])
				->first();

			if($data) {
				$item['name'] = $data->name;
				$item['price'] = $data->pvp;
				Cart::add($item);

				return $this->index();
			}
			else{
				return Response::json('Invalid',500);
			}
		}
		else{	
			return Response::json( $validator->messages(),202 );
		}
	}

	public function show($id)
	{
		return Response::json( Cart::get($id),200 );
	}

	public function edit($id)
	{

	}

	public function update($id)
	{
		$qty = Input::get('qty');

		Cart::update($id,$qty);

		return Response::json( Cart::get($id),200 );
	}

	public function destroy($id)
	{
		Cart::destroy();

		return $this->index();
	}

}