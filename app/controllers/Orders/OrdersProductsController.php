<?php

class OrdersProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Auth::check())
		{
			$order = Input::get('order');
			$item = Orders::find($order);

			$orders_products = new OrdersProducts;
			$product = new Products;

			$data = array(
				'order' => $item->id = $item->id
				'product' => $orders_products->product = Input::get('product'),
				'pvp' =>  $orders_products->pvp = Input::get('pvp'),
				'total' =>  $orders_products->total = Input::get('total'),
				'qty' =>  $orders_products->qty = Input::get('qty'),
			);

			$product = $product->find($data['product']);

			$data['pvp'] = $product->pvp;

			$validator = Validator::make(
				$data,
				array(
					'order' => 'required',
					'product' => 'required',
					'pvp' => 'required',
					'total' => 'required',
					'qty' => 'required',
				)
			);
			if( $validator->passes() ){
				if($orders_products->save()){
					return Response::json(
						$orders_products,
						202
					);
				}
				else
				{
					return Response::json(
						$orders_products,
						500
					);	
				}
			}
			else{
				return Response::json(
					$validator->messages(),
					403
				);	
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}