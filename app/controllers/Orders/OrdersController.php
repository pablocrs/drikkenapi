<?php

class OrdersController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth');

        $this->beforeFilter('csrf', array('on' => 'post'));
    }


	public function index()
	{
		$orders = new Orders;

		$id = Input::get('id');
		$active = Input::get('active');
        $user =  Input::get('user');
        $vendor = Input::get('vendor');

        switch(Auth::user()->type) {
            case 1:
                $data = $orders->getByVendor($id,$user,$vendor,$active);
            break;
            case 2:
                $data = $orders->getByVendor($id,$user,Auth::user()->email,$active);
            break;
            case 3:
                $data = $orders->getById($id,Auth::user()->email,$active);
            break;
            default:
                return Response::json('Authentication failed',404);
            break;
        }

		return Response::json(
			$data,
			202
		);
	}


	public function create()
	{
	
	}


	public function store()
	{
		$order_location = new Location;
        $order = new Orders;
        $not = new Notifications;

        $location = $order_location->save_location();

        $data = array(
            'user' => $order->user = Auth::user()->email,
            'phone' => $order->phone = Input::get('phone'),
            'postalcode' => $order->postalcode = Input::get('postalcode'),
            'address' => $order->address = Input::get('address'),
            'zone' => $order->zone = Input::get('zone'),
            'comments' => $order->comments = Input::get('comments','Sin comentarios'),
            'payment' => $order->payment = Input::get('payment'),
            'prepaid' => $order->prepaid = Input::get('prepaid'),
            'delivery' => $order->delivery = Input::get('delivery'),
            'location' => $order->location = $location->id,
            'vendor' => $order->vendor = Input::get('vendor'),
            'status' => $order->vendor = Input::get('vendor','new')
        );

        $validator = Validator::make(
            $data,
            array(
                'phone' => 'required',
                'postalcode' => 'required',
                'address' => 'required',
                'zone' => 'required',
                'payment' => 'required',
                'prepaid' => 'required',
                'delivery' => 'required',
                'user' =>'required|email',
                'location' =>'required',
                'vendor' =>'required|email'
            )
        );

        if($validator->passes()){
            if($order->save()){
                $not->add("Nuevo pedido",$order->id,'orders',$order->vendor);
                // TODO: SEND EMAILS
                return Response::json(
                    $order,
                    202
                );
            }
            else{
                return Response::json(
                    'Server error',
                    500
                );
            }
        }

        else{
            return Response::json(
                $validator->messages(),
                403
            );
        }
	}


	public function show($user)
	{
		$orders = new Orders;
		$id = Input::get('id');
		$active = Input::get('active');
        $vendor = Input::get('vendor');

        switch(Auth::user()->type) {
            case 1:
                $data = $orders->getByVendor($id,$user,$vendor,$active);
            break;
            case 2:
                $data = $orders->getByVendor($id,$user,Auth::user()->email,$active);
            break;
            case 3:
                $data = $orders->getById($id,Auth::user()->email,$active);
            break;
            default:
                return Response::json('Authentication failed',404);
            break;
        }

        return Response::json(
            $data,
            202
        );
	}


	public function edit($id)
	{
		// TODO: ADMIN EDIT
	}


	public function update($id)
	{
		// TODO: UPDATE STATUS VENDOR
	}


	public function destroy($id)
	{
		// TODO: ADMIN DESTROY
	}

}