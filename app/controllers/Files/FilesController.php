<?php
/// TODO: BORRAR CARPETA VACÍA
class FilesController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth.vendor');
    }

	public function index()
	{
        if(Input::get('flowIdentifier'))
        {
            $temp_dir = 'temp/'.Input::get('flowIdentifier');
            $chunk_file = $temp_dir.'/'.Input::get('flowFilename').'.part'.Input::get('flowChunkNumber');

            if (file_exists($chunk_file))
            {
                return Response::json($chunk_file,200);
            }
            else
            {
                return Response::json($chunk_file,404);
            }
        }
        else
        {
            $dir = 'img/'.Auth::user()->email;
            if(is_dir($dir)){
                $objects = array_diff(scandir($dir,1), array('..', '.'));
                $data = array();
                foreach($objects as $key => $value){
                    $fileurl = $dir.'/'.$value;
                    $data[$key] = array(
                        'id' => $value,
                        'type' => pathinfo($fileurl,PATHINFO_EXTENSION),
                        'size' => filesize($fileurl),
                        'url' => $fileurl
                    );
                }
                return Response::json($data,200);
            }
        }
	}


	public function create()
	{

	}


	public function store()
	{
        if (!empty($_FILES)) foreach ($_FILES as $file) {

            if (Input::file('error') != 0) {
                return Response::json('FILE ERROR',500);
            }

            $temp_dir = 'temp/'.Input::get('flowIdentifier');
            $dest_file = $temp_dir.'/'.Input::get('flowFilename').'.part'.Input::get('flowChunkNumber');
            $final_dir = 'img/'.Auth::user()->email;

            if (!is_dir($temp_dir)) {
                mkdir($temp_dir, 0777, true);
            }
            if (!is_dir($final_dir)) {
                mkdir($final_dir, 0777, true);
            }

            $files = new Files;
            if(!move_uploaded_file($file['tmp_name'],$dest_file))
            {
                return Response::json('Error saving (move_uploaded_file) chunk '.Input::get('flowChunkNumber').' for file '.Input::get('flowFilename'),500);
            }
            else
            {
                $files->createFileFromChunks($temp_dir, Input::get('flowFilename'),Input::get('flowChunkSize'), Input::get('flowTotalSize'), $final_dir);
            }
        }
        return null;
	}


	public function show($id)
	{
		//
	}


	public function edit($id)
	{
		//
	}


	public function update($id)
	{
		//
	}


	public function destroy($file)
	{
        if($file)
        {
            $dir = 'img/'.Auth::user()->email.'/'.$file;
            unlink($dir);
            return Response::json('Unlink',200);

        }
        else
        {
            return Response::json('No name',403);
        }
	}

}