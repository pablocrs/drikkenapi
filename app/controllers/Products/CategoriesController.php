<?php

class CategoriesController extends BaseController {


    public function __construct()
    {
        $this->beforeFilter('auth.vendor',array('except' => array('index','show')));
        $this->beforeFilter('csrf', array('except' => array('index','show')));
    }

	public function index()
	{	
		$categories = new Categories;
		if(Input::get('mode')==='ord'){
			$data = $categories->get_ord();
		}
		else{
			$data = $categories->all();
		}
		return Response::json(
			$data,
			202
		);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if( Auth::check() && Auth::user()->type <= 2 )
		{
			$category = new Categories;

			$validator = Validator::make(
				array(
					'name' => $category->name = Input::get('name'),
					'description' => $category->description = Input::get('description'),
					'parent' => $category->parent = Input::get('parent'),
					'user' => $category->user = Auth::user()->email
				),
				array(
					'name' => 'required',
					'parent' => 'required',
					'user' => 'required',
				)
			);
			if( $validator->passes() )
			{				
				if( $data = $category->save() )
				{
					return Response::json(
						$category,
						201
					);								
				}
				else
				{
					return Response::json(
						'Database error ProductsController->Store',
						500
					);	
				}
			}
			else
			{
				return Response::json(
					$validator->messages(),
					403
				);	
			}
		}
		else
		{
			return Response::json(
				'Forbidden',
				403
			);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if( Auth::check() && Auth::user()->type <= 2 )
		{
			$category = new Categories;

			$validator = Validator::make(
				array(
					'name' => $category->name = Input::get('name'),
					'description' => $category->description = Input::get('description'),
					'parent' => $category->parent = Input::get('parent'),
					'url' => $category->url = Input::get('url'),
					'user' => $category->user = Auth::user()->email
				),
				array(
					'name' => 'required',
					'parent' => 'required',
					'user' => 'required',
				)
			);
			if( $validator->passes() )
			{				
				if( $data = $category->save() )
				{
					return Response::json(
						$category,
						201
					);								
				}
				else
				{
					return Response::json(
						'Database error ProductsController->Store',
						500
					);	
				}
			}
			else
			{
				return Response::json(
					$validator->messages(),
					403
				);	
			}
		}
		else
		{
			return Response::json(
				'Forbidden',
				403
			);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data = Categories::where('user','=',$id)->get();
		return Response::json(
			$data,
			202
		);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item = Categories::find($id);
		$item->delete();
		return Response::json(
			$item,
			200
		);
	}

}