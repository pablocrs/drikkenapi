<?php

class ProductsController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth.vendor',array('except' =>
                                             array('index','show')));
        $this->beforeFilter('csrf', array('except' =>
                                            array('index','show')));
    }

	public function index()
	{
		$products_model = new Product;

		$data = $products_model->get_by(
			$id = Input::get('id'),
			$important = Input::get('important'),
			$discount = Input::get('discount'),
			$category = Input::get('category'),
			$user =  Input::get('user'),
			$tag = Input::get('tag'),
			$zone = Input::get('zone' ),
			$stock = Input::get('stock'),
			$url = Input::get('url'),
			$orderby = Input::get('orderby'),
			$ordermode = Input::get('ordermode'),
			$limit = Input::get('limit'),
			$page = Input::get('page'),
			$postalc = Input::get('postalc')
		);	

		return Response::json(
			$data,
			202
		);
	}

	public function create()
	{
		//	
	}

	public function store()
	{
        $product = new Product;

        $validator = Validator::make(
            array(
                'name' => $product->name = Input::get('name'),
                'important' => $product->important = Input::get('important',false),
                'discount' => $product->discount = Input::get('discount',false),
                'price' => $product->price = Input::get('price'),
                'pvr' => $product->pvr = Input::get('pvr',false),
                'pvp' => $product->pvp = Input::get('pvp'),
                'description' => $product->description = Input::get('description',false),
                'user' => $product->user = Auth::user()->email,
                'stock' => $product->stock = Input::get('stock',false),
                'url' => $product->url = Input::get('url')
            ),
            array(
                'name' => 'required',
                'price' => 'required',
                'pvp' => 'required',
                'user' =>'required|email',
                'url' =>	'required'
            )
        );

        if( $validator->passes() )
        {
            if($product->save())
            {
                return Response::json(
                    $product,
                    201
                );
            }
            else
            {
                return Response::json(
                    'Database error ProductsController->Store',
                    500
                );
            }
        }
        else
        {
            return Response::json(
                $validator->messages(),
                403
            );
        }
	}

	public function show($url)
	{
		$products_model = new Product;

		$data = $products_model->get_by(
			$id = Input::get('id'),
			$important = Input::get('important'),
			$discount = Input::get('discount'),
			$category = Input::get('category'),
			$user = Input::get('user'),
			$tag = Input::get('tag'),
			$zone = Input::get('zone' ),
			$stock = Input::get('stock'),
			$url,
			$orderby = Input::get('orderby'),
			$ordermode = Input::get('ordermode'),
			$limit = Input::get('limit'),
			$page = Input::get('page'),
			$postalc = Input::get('postalc')
		);	

		return Response::json(
			$data,
			202
		);
	}

	public function edit($id)
	{
		return $id;
	}

	public function update($id)
	{
		//
	}

	public function destroy($id)
	{
		$item = Product::find($id);
		$item->delete();
		return Response::json(
			$item,
			200
		);
	}

}