<?php

class ProductsCategoriesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$productCategories = new ProductCategories;
		$data = $productCategories->all();
		return Response::json(
			$data,
			202
		);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if( Auth::check() && Auth::user()->type <= 2 )
		{
			$productCategories = new ProductCategories;

			$validator = Validator::make(
				array(
					'product' => $productCategories->product = Input::get('product'),
					'category' => $productCategories->category = Input::get('category',false),
					'user' => $productCategories->user = Auth::user()->email
				),
				array(
					'product' => 'required',
					'category' =>	'required',
					'user' =>'required|email'
				)
			);

			if( $validator->passes() )
			{	
				if( $productCategories->save() )
				{
					return Response::json(
						$productCategories,
						201
					);								
				}
				else
				{
					return Response::json(
						'Database error ProductsController->Store',
						500
					);	
				}
			}
			else
			{
				return Response::json(
					$validator->messages(),
					403
				);	
			}
		}	
		else
		{
			return Response::json(
				'Forbidden',
				403
			);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if( Auth::check() && Auth::user()->type <= 2 )
		{
			$productCategories = new ProductCategories;

			$validator = Validator::make(
				array(
					'product' => $productCategories->product = Input::get('product'),
					'category' => $productCategories->category = Input::get('category',false),
					'user' => $productCategories->user = Auth::user()->email
				),
				array(
					'product' => 'required',
					'category' =>	'required',
					'user' =>'required|email'
				)
			);

			if( $validator->passes() )
			{	
				if( $productCategories->save() )
				{
					return Response::json(
						$productCategories,
						201
					);								
				}
				else
				{
					return Response::json(
						'Database error ProductsController->Store',
						500
					);	
				}
			}
			else
			{
				return Response::json(
					$validator->messages(),
					403
				);	
			}
		}	
		else
		{
			return Response::json(
				'Forbidden',
				403
			);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $item = ProductCategories::find($id);
        $item->delete();
        return Response::json(
            $item,
            200
        );
	}

}