<?php

class ProductsImagesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $productsImages = new ProductImages;
        $data = $productsImages->all();
        return Response::json(
            $data,
            202
        );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        if( Auth::check() && Auth::user()->type <= 2 )
        {
            $productsImages = new ProductImages;

            $validator = Validator::make(
                array(
                    'product' => $productsImages->product = Input::get('product'),
                    'image' => $productsImages->image = Input::get('image'),
                    'user' => $productsImages->user = Auth::user()->email
                ),
                array(
                    'product' => 'required',
                    'image' =>	'required',
                    'user' =>'required|email'
                )
            );

            if( $validator->passes() )
            {
                if( $productsImages->save() )
                {
                    return Response::json(
                        $productsImages,
                        201
                    );
                }
                else
                {
                    return Response::json(
                        'Database error ProductsImages',
                        500
                    );
                }
            }
            else
            {
                return Response::json(
                    $validator->messages(),
                    403
                );
            }
        }
        else
        {
            return Response::json(
                'Forbidden',
                403
            );
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $item = DB::table('products_images')->where('image', '=', $id);
        $item->delete();
        return Response::json(
            $item,
            200
        );
	}

}