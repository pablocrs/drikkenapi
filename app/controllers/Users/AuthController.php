<?php

class AuthController extends BaseController {


    public function __construct()
    {
        $this->beforeFilter('auth',array('except' => 'store'));
        $this->beforeFilter('auth.admin',array('only' => array('update','destroy')));
    }


	public function index()
	{
        $data = array(
            'user' => Auth::user()->toArray(),
            'token' => Session::token()
        );
        return Response::json(
            $data,
            200
        );
	}


	public function create()
	{
        $user = new User;
        $not = new Notifications;
        $zone = new Zones;
        $location = new Location;

        $validator = Validator::make(
            array(
                'name' => $user->name = Input::get('name',false),
                'email' => $user->email = Input::get('email'),
                'password' => $user->password = Input::get('password'),
                'type' => $user->type = Input::get('type',false),
                'zone' => $user->zone = Input::get('zone',false),
                'postalcode' => $user->postalcode = Input::get('postalcode',false),
                'address' => $user->address = Input::get('address',false),
                'phone' => $user->phone = Input::get('phone',false),
                'image' => $user->image = Input::get('phone',false),
                'token' => $user->token = Session::token()
            ),
            array(
                'email' =>'required|email',
                'password' =>'required',
            )
        );
        if( $validator->passes() ){

            if( $user->save() )
            {
                $location = $location->save_location($user->email);

                if($user->postalcode){
                    $vendor = $zone->getByPc($user->postalcode);
                }
                else{
                    $vendor = $zone->getByPc($location->postal_code);
                }
                if($vendor){
                    $not->add("Nuevo usuario en zona",$user->email,'users',$vendor->email);
                }

                // TODO: SEND TOKEN EMAIL

                return Response::json(
                    $user,
                    201
                );

            }
            else{
                return Response::json(
                    'Server error',
                    500
                );
            }

        }
        else{
            return Response::json(
                $validator->messages(),
                500
            );
        }
	}


	public function store()
	{
        $credentials = array(
			'email' =>  Input::get('email'),
			'password' =>  Input::get('password'),
		);

        if(Auth::attempt($credentials)) {
            $response = Response::json(array(
				'user' => Auth::user()->toArray(),
                'token' => Session::token()
                ),
				202
			);

            return $response->header('_token',Session::token());
		}
		else{
			return Response::json(
                'Authentication failed',
				    401
			);
		}
	}


	public function show($email)
	{
		// TODO: GET USER DATA
	}


	public function edit($email)
	{
		// TODO: EDIT USER DATA
	}


	public function update($id)
	{
		// TODO: EDIT USER DATA ADMIN
	}


	public function destroy($id)
	{
		// TODO: REMOVE USER DATA ADMIN
	}

}