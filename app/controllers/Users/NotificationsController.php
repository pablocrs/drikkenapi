<?php

class NotificationsController extends BaseController {

    public function __construct()
    {
        $this->beforeFilter('auth.vendor');
        $this->beforeFilter('csrf', array('except' =>
            array('index','show')));
    }

	public function index()
	{
        $user = Auth::user()->email;
        $data = Notifications::where('user','=',$user)->orderBy('updated_at','desc')->get();

        return Response::json(
            $data,
            202
        );
	} 


	public function create()
	{

	}


	public function store()
	{
		$notification = new Notifications;

		$notification->title = Input::get('titulo');
		$notification->link = Input::get('enlace');
		$notification->created = Input::get('created');
		$notification->user = Input::get('user');

		if($notification->save()){
            return Response::json(
                notification,
                203
            );
		}
		else{
            return Response::json(
                $notification,
                500
            );
		}
	}


	public function show($id)
	{
        $user = Input::get('user');

        switch(Auth::user()->type){
            case 1:
                Notifications::where('user','=',$user)->where('id','=',$id);
            break;
            case 2:
                Notifications::where('user','=',Auth::user()->email)->where('id','=',$id);
            break;
        }

        $data = Notifications::orderBy('updated_at','desc')->get();

        return Response::json(
            $data,
            202
        );
	}


	public function edit($id)
	{
		// TODO: SOFT DELETE
	}


	public function update($id)
	{

	}


	public function destroy($id)
	{
        $notification = Notifications::find($id);
        $notification->delete();

		return Response::json(
            $notification,
			200
		);
	}

}