<?php

class Postalcodes extends Eloquent {

	protected $table = 'postalcodes';

	public function getByPc($id){

		$zone = DB::table('postalcodes');
		$zone->select(
			'postalcodes.id as id',
			'postalcodes.number as number',
			'zones.name as zone',
			'zones.description as description',
			'postalcodes.number as url',
			'postalcodes.created_at as created_at',
			'postalcodes.updated_at as updated_at'
			);
		$zone->where('number',$id);

		$zone->leftJoin('zones','postalcodes.zone','=','zones.id');

		$items = $zone->get();
		return $items;
	}

	public function getByUser($id){

		$zone = DB::table('postalcodes');
		$zone->select(
			'postalcodes.id as id',
			'postalcodes.number as number',
			'zones.name as zone',
			'zones.description as description',
			'postalcodes.number as url',
			'postalcodes.created_at as created_at',
			'postalcodes.updated_at as updated_at'
			);
		$zone->leftJoin('zones','postalcodes.zone','=','zones.id');

		$items = $zone->get();
		return $items;
	}
}