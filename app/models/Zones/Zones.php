<?php

class Zones extends Eloquent {
	
	protected $table = 'zones';

	public function getByPc($id){
		
		$zone = DB::table('zones');
		$zone->select(
			"zones.id as id",
			"zones.name as name",
			"zones.url as url",
			"zones.description as description",
			"zones.description as description",
			"postalcodes.number as postalcode",
			"users.email as email",
			"users.name as username",
			"users.phone as userphone",
			"users.address as useraddress"
			);

		$zone->leftJoin('postalcodes','postalcodes.zone','=','zones.id');
		$zone->leftJoin('users','postalcodes.user','=','users.email');

		$zone->where('postalcodes.number',$id);
		
		$items = $zone->get();
		return $items;
	}

	public function getByUser($id){

		$zone = DB::table('zones');
		$zone->select(
			"zones.id as id",
			"zones.name as name",
			"zones.name as description",
			"zones.url as url",
			"zones.created_at",
			"zones.updated_at"
			);
		$zone->where('user',$id);

		$items = $zone->get();
		return $items;
	}
}