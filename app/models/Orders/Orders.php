<?php

class Orders extends Eloquent {
	
	protected $table = 'orders';

	public function getById($id,$user,$active = null){
		
		$zone = DB::table('orders');
		$zone->select(
			"orders.id",
			"orders.user",
			"orders.phone",
			"orders.vendor",
			"orders.postalcode",
			"orders.address",
			"orders.zone",
			"orders.vendor",
			"orders.delivery",
			"orders.created_at",
            "orders.status"
		);

		$zone->where('orders.id',$id);
		$zone->where('orders.user',$user);
		
		$items = $zone->get();

		$items = $this->setProducts($items);

		return $items;
	}

	public function getByUser($user,$active = null){

		$orders = DB::table('orders');
		$orders->select(
			"orders.id",
			"orders.user",
			"orders.phone",
			"orders.vendor",
			"orders.postalcode",
			"orders.address",
			"orders.zone",
			"orders.vendor",
			"orders.delivery",
			"orders.created_at",
            "orders.status"
		);

		$orders->where('user',$user);

		if($active == true){
			$orders->where('active',true);
		}
		if($active == false){
			$orders->where('active',true);
		}

		$items = $orders->get();

		$items = $this->setProducts($items);

		return $items;
	}

	public function getByVendor($id,$user,$vendor,$active){

		$orders = DB::table('orders');
		$orders->select(
			"*"
		);

		if($id != null){
			$orders->where('id',$id);
		}
		if($user != null){
			$orders->where('user',$user);
		}
		if($vendor != null){
			$orders->where('vendor',$vendor);
		}
		if($active == true){
			$orders->where('active',true);
		}
		if($active == false){
			$orders->where('active',true);
		}


		$items = $orders->get();

		$items = $this->setLocation($items);
		$items = $this->setProducts($items);

		return $items;
	}

	private function setProducts($items){
		foreach ($items as $item)
		{
			$orders_products =  DB::table('orders_products');
			$orders_products->select('*');
			$orders_products->where('order',$item->id);
			$orders_products->leftJoin('products', 'products.id','=','orders_products.product');
			$orders_products = $orders_products->get();

			$item->products = $orders_products;
		}
		return $items;
	}

	private function setLocation($items){
		foreach ($items as $item)
		{
			$location =  DB::table('location');
			$location->select('*');
			$location->where('id',$item->location);

			$location = $location->get();

			$item->location = $location;
		}
		return $items;
	}
}