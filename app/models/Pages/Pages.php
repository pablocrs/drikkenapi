<?php

class Pages extends Eloquent {

    protected $table = 'pages';

    public function getByUrl($url,$user){

        $page = DB::table('pages');

        $page->select(
           '*'
        );

        $page->where('url','=',$url);

        if($user != FALSE){
            $page->where('user','=',$user);
        }

        $data = $page->first();

        if($data){

            $texts =  DB::table('texts');
            $texts->where('page','=',$data->url);
            $texts = $texts->get();

            $data->texts = array();

            foreach ($texts as $item){
                $data->texts[$item->name] = $item->text;
            }
        }
        else{

            $data = false;

        }

        return $data;
    }

    public function getByName($name,$lang = FALSE){

        $page = DB::table('pages');

        $page->select(
            '*'
        );

        $page->where('name','=',$name);
        if($lang != FALSE){
            $page->where('lang','=',$lang);
        }

        $data = $page->first();

        $texts =  DB::table('texts');
        $texts->where('page','=',$data->url);
        $texts = $texts->get();

        $data->texts = array();

        foreach ($texts as $item){
            $data->texts[$item->name] = $item->text;
        }

        return $data;
    }
}