<?php

class Product extends Eloquent{
    
    	protected $table = 'products';
	protected $hidden = array('password');
   	protected $fillable = array('password', 'email');

	public function get_by(
		$id=NULL,
		$important=NULL,
		$discount=NULL,
		$category=NULL,
		$user=NULL,
		$tag=NULL,
		$zone=NULL,
		$stock=NULL,
		$url=NULL,
		$orderby=NULL,
		$ordermode=NULL,
		$limit=NULL,
		$page=NULL,
		$postalc=NULL
		)
	{
		$products = DB::table('products');

		$products->select(
			"products.id as id",
			"products.name",
			"products.description",
			"products.important",
			"products.discount",
			"products.stock",
			"products.price",
			"products.pvp",
			"products.pvr",
			"products.user",
			"products.user",
			"products.url",
			"products.created_at",
			"users.name as user_name"
		);
		
		$products->leftJoin('users', 'products.user','=','users.email');

		if($id != NULL)
		{
			$products->where('products.id', $id);
		}
		if($postalc != NULL)
		{
			$products->leftJoin('postalcodes', 'postalcodes.user','=','products.user');
			$products->where('postalcodes.number', $postalc);
		}
		if($important != NULL)
		{
			$products->where('products.important', $important);
		}
		if($discount != NULL)
		{
			$products->where('products.discount','<=', $discount);
		}
		if($category != NULL)
		{
			$products->addSelect(			
				"categories.name as categoy_name",
				"products_categories.category as categoy_id"
			);
			$products->leftJoin('products_categories', 'products.id','=','products_categories.product');
			$products->leftJoin('categories', 'categories.id','=','products_categories.category');
			$products->where('categories.url', $category);
		}	
		if($tag != NULL)
		{
			$products->leftJoin('products_tags', 'products_tags.product','=','products.id');
			$products->leftJoin('tags', 'tags.id','=','products_tags.tag');
			$products->where('tags.url', $tag);
		}
		if($zone != NULL)
		{
			$products->leftJoin('users_zones', 'users_zones.user','=','products.user');
			$products->leftJoin('zones', 'zones.id','=','users_zones.zone');
			$products->where('zones.url', $zone);
		}
		if($stock != NULL)
		{
			$products->where('products.stock', $stock);
		}
		if($user != NULL)
		{
			$products->where('products.user', $user);
		}
		if($url != NULL)
		{
			$products->where('products.url', $url);
		}
		if($orderby != NULL)
		{
			if ($ordermode === NULL) {$ordermode = 'asc';};
			$products->orderBy($orderby,$ordermode); 
		 }
		 if ($limit != NULL)
		{
			if ($page === NULL) {$page = '0';};
		 	$products->skip($limit)->take($page);
		}

		$items = $products->get();

		foreach ($items as $item)
		{
			$products_img =  DB::table('products_images');
			$products_img->select('*');
			$products_img->where('product',$item->id);
			$image = $products_img->get();

			$item->images = $image;
		}

		foreach ($items as $item)
		{
			$products_tags =  DB::table('products_tags');
			$products_tags->select('*');
			$products_tags->where('product',$item->id);
			$products_tags->leftJoin('tags', 'tags.id','=','products_tags.tag');
			$tag = $products_tags->get();

			$item->tags = $tag;
		}
		
		foreach ($items as $item)
		{
			$products_categories =  DB::table('products_categories');
			$products_categories->select('*');
			$products_categories->where('products_categories.product',$item->id);
			$products_categories->leftJoin('categories', 'categories.id','=','products_categories.category');
			$category = $products_categories->get();

			$item->categories = $category;
		}
		
		return $items;
	}

}