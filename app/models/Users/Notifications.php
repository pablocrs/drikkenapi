<?php

class Notifications extends Eloquent {

	protected $table = 'notifications';

	public function add($title,$link,$type,$user){

		$notification = new Notifications;

		$notification->title = $title;
		$notification->type = $type;
		$notification->link = $link;
		$notification->user = $user;

		if($notification->save()){
			return true;
		}
		else{
			return false;
		}
	}

}