<?php

class Location extends \Eloquent {

	protected $table = 'location';

	public function save_location($user = false){
		
		$order_location = new Location;
		$location = GeoIP::getLocation();

		$order_location->ip = $location['ip'];
		$order_location->isoCode = $location['isoCode'];
		$order_location->country = $location['country'];
		$order_location->city = $location['city'];
		$order_location->state = $location['state'];
		$order_location->postal_code = $location['postal_code'];
		$order_location->latitude = $location['lat'];
		$order_location->longitude = $location['lon'];

        if($user){
            $order_location->user = $user;
        }

		if($order_location->save()){
			return $order_location;
		}
	}
}