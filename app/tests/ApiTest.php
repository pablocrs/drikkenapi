<?php

class ApiTest extends TestCase {

	public function setUp()
	{
		parent::setUp();

		Route::enableFilters();

		Artisan::call('migrate');
		Artisan::call('db:seed');

		Auth::loginUsingId(1);

		// OR:
		//$this->be(User::find(1));
	}

	public function testIndex()
	{
		$crawler = $this->client->request('GET', '/api');

		$this->assertTrue($this->client->getResponse()->isOk());
		$data = $this->parseJson($response);
	}

	public function testProducts()
	{
		$crawler = $this->client->request('GET','/api/products');
		
		$this->assertTrue($this->client->getResponse()->isOk());
		$data = $this->parseJson($response);		
	}
}