<?php

class ProductstagsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('products_tags')->delete();

		ProductTags::create(array(
			'product' => '1',
			'tag' => '1',
		));
		ProductTags::create(array(
			'product' => '2',
			'tag' => '2',
		));
		ProductTags::create(array(
			'product' => '3',
			'tag' => '3',
		));
	}

}