<?php

class UsersZonesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users_zones')->delete();

		UsersZones::create(array(
			'user' => 'vendor@vendor.com',
			'zone' => '1',
		));

		UsersZones::create(array(
			'user' => 'vendor@vendor.com',
			'zone' => '2',
		));

		UsersZones::create(array(
			'user' => 'vendor@vendor.com',
			'zone' => '3',
		));
	}
}

?>