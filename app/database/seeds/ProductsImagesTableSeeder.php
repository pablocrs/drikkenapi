<?php

class ProductsImagesTableSeeder extends Seeder {

	public function run()
	{
        DB::table('products_images')->delete();

        ProductImages::create(array(
            'product' => '1',
            'image' => 'http://placekitten.com/100/150',
            'user' => 'vendor@vendor.com',
        ));

        ProductImages::create(array(
            'product' => '1',
            'image' => 'http://placekitten.com/100/150',
            'user' => 'vendor@vendor.com',
        ));

        ProductImages::create(array(
            'product' => '2',
            'image' => 'http://placekitten.com/100/150',
            'user' => 'vendor@vendor.com',
        ));
	}

}