<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// USUARIOS
		$this->call('UserTableSeeder');

		// PRODUCTOS
		$this->call('ProductsTableSeeder');
		// IMAGENES
		$this->call('ImagesTableSeeder');
		// CATEGORIAS
		$this->call('CategoriesTableSeeder');
		// ZONAS
		$this->call('ZonesTableSeeder');
		// TAGS
		$this->call('TagsTableSeeder');
		// PRODUCTOS -> IMAGENES
		$this->call('ProductsImagesTableSeeder');
		// PRODUCTOS -> CATEGORIAS
		$this->call('ProductscategoriesTableSeeder');
		// PRODUCTOS -> TAGS
		$this->call('ProductstagsTableSeeder');

		// CODIGOS POSTALES
		$this->call('PostalcodesTableSeeder');
		// USUARIOS->CODIGOS POSTALES
		$this->call('UsersZonesTableSeeder');	

		//ODERS
		$this->call('OrdersTableSeeder');	
		$this->call('OrdersProductsTableSeeder');	
		$this->call('LocationTableSeeder');	
		$this->call('NotificationsTableSeeder');

        //PAGES
		$this->call('PagesTableSeeder');
		$this->call('TextsTableSeeder');
	}

}