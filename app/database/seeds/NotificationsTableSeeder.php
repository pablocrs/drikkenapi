<?php

class NotificationsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('notifications')->delete();

		Notifications::create(array (
			"title"           => "Nuevo mensaje",
			"type"      => "messages",
			"link"         => "1",
			"user"        => "vendor@vendor.com",
		));

		Notifications::create(array (
			"title"           => "Nuevo pedido",
			"type"      => "orders",
			"link"         => "1",
			"user"        => "vendor@vendor.com",
		));

		Notifications::create(array (
			"title"           => "Producto agotado",
			"type"      => "products",
			"link"         => "Protos",
			"user"        => "vendor@vendor.com",
		));
	}

}