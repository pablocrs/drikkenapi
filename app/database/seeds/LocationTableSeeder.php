<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LocationTableSeeder extends Seeder {

	public function run()
	{
		DB::table('location')->delete();

		Location::create(array (
			"ip"           => "232.223.11.11",
			"isoCode"      => "US",
			"country"      => "United States",
			"city"         => "New Haven",
			"state"        => "CT",
			"postal_code"  => "06510",
			"latitude"     => 36.726080,
			"longitude"    => -4.431679
		));

        Location::create(array (
            "ip"           => "232.223.11.11",
            "isoCode"      => "US",
            "country"      => "United States",
            "city"         => "New Haven",
            "state"        => "CT",
            "postal_code"  => "06510",
            "latitude"     => 36.725804,
            "longitude"    => -4.420092
        ));

        Location::create(array (
            "ip"           => "232.223.11.11",
            "isoCode"      => "US",
            "country"      => "United States",
            "city"         => "New Haven",
            "state"        => "CT",
            "postal_code"  => "06510",
            "latitude"     => 36.718374,
            "longitude"    => -4.428675
        ));
	}

}