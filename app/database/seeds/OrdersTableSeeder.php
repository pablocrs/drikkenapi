<?php

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('orders')->delete();

		Orders::create(array(

			'user' => 'user@user.com',
			'phone' => '648615924',
			'postalcode' => '29015',
			'zone' => 'Malaga',
			'address' => 'C/Carreteria nº3 4ºb',
			'comments' => 'Cambio de 50',

			'payment' => 'contrareembolso',
			'prepaid' => false,
			'delivery' => '18/08/14',

			'price' => '20',
			'pvp' => '28',

			'location' => 2,

			'vendor' => 'vendor@vendor.com',
			'active' => true,
            'status' => 'new'
		));

		Orders::create(array(

			'user' => 'user@user.com',
			'phone' => '648615924',
			'postalcode' => '29015',
			'zone' => 'Malaga',
			'address' => 'C/Carreteria nº3 4ºb',
			'comments' => 'Cambio de 50',

			'payment' => 'contrareembolso',
			'prepaid' => false,
			'delivery' => '18/08/14',

			'price' => '15',
			'pvp' => '25',

			'location' => 3,

			'vendor' => 'vendor@vendor.com',
			'active' => true,
            'status' => 'new'
		));

		Orders::create(array(

			'user' => 'user@user.com',
			'phone' => '654587984',
			'postalcode' => '29015',
			'zone' => 'Malaga',
			'address' => 'C/Lagunillas nº3 4ºb',
			'comments' => 'Traer antes de las 4',

			'payment' => 'contrareembolso',
			'prepaid' => false,
			'delivery' => '15/08/14',
			
			'price' => '10',
			'pvp' => '15',

			'location' =>1,

			'vendor' => 'vendor@vendor.com',
			'active' => true,
            'status' => 'new'
		));

		Orders::create(array(

			'user' => 'user@user.com',
			'phone' => '654587984',
			'postalcode' => '29015',
			'zone' => 'Malaga',
			'address' => 'C/Lagunillas nº3 4ºb',
			'comments' => 'Traer antes de las 4',

			'payment' => 'contrareembolso',
			'prepaid' => false,
			'delivery' => '15/08/14',
			
			'price' => '35',
			'pvp' => '40',

			'location' =>2,

			'vendor' => 'vendor@vendor.com',
			'active' => true,
            'status' => 'confirmed'
		));

		Orders::create(array(

			'user' => 'user@user.com',
			'phone' => '654587984',
			'postalcode' => '29015',
			'zone' => 'Malaga',
			'address' => 'C/Lagunillas nº3 4ºb',
			'comments' => 'Traer antes de las 4',

			'payment' => 'contrareembolso',
			'prepaid' => false,
			'delivery' => '15/08/14',
			
			'price' => '28',
			'pvp' => '32',

			'location' =>3,

			'vendor' => 'vendor@vendor.com',
			'active' => true,
            'status' => 'confirmed'
		));
	}
}

?>