<?php

class ProductsTableSeeder extends Seeder {
	
	public function run(){

		DB::table('products')->delete();

		Product::create(array(
			'name' => 'Protos',
			'important' => true,
			'description' => '<p>Descripcion</p>',
			'discount' => '30',
			'stock' => 0,
			'price' => 14,
			'pvp' => 25,
			'pvr' => 24,
			'user' => 'vendor@vendor.com',
			'url' => 'Protos',
		));

		Product::create(array(
			'name' => 'Chivas',
			'important' => false,
			'description' => '<p>Descripcion</p>',
			'discount' => '30',
			'stock' => 10,
			'price' => 25,
			'pvp' => 18,
			'pvr' => 17,
			'user' => 'vendor@vendor.com',
			'url' => 'Chivas-Regal',
		));


		Product::create(array(
			'name' => 'Smirnoff',
			'important' => true,
			'description' => '<p>Descripcion</p>',
			'discount' => '12',
			'stock' => 50,
			'price' => 6,
			'pvp' => 10,
			'pvr' => 9,
			'user' => 'vendor@vendor.com',
			'url' => 'Smirnoff',
		));
	}
}

?>