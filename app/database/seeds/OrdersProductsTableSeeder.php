<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrdersProductsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('orders_products')->delete();

		OrdersProducts::create(array(
			'order' => 1,
			'product' => 1,
			'qty' => 3
		));
		OrdersProducts::create(array(
			'order' => 1,
			'product' => 2,
			'qty' => 7
		));
		OrdersProducts::create(array(
			'order' => 2,
			'product' => 3,
			'qty' => 13
		));
	}

}