<?php

class TagsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('tags')->delete();

		Tags::create(array(
			'name' => 'Rioja',
			'url' => 'Rioja',
			'user' => 'vendor@vendor.com',
		));
		Tags::create(array(
			'name' => 'Escoceses',
			'url' => 'Escoceses',
			'user' => 'vendor@vendor.com',
		));
		Tags::create(array(
			'name' => 'Smirnoff',
			'url' => 'Smirnoff',
			'user' => 'vendor@vendor.com',
		));
	}

}