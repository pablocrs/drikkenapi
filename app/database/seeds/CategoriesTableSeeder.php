<?php

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categories')->delete();

		Categories::create(array(
			'id' => 1,
			'name' => 'Vinos',
			'description' => 'Text',
			'parent' => '0',
			'user' => 'vendor@vendor.com',
			'url' => 'Vinos',
		));

		Categories::create(array(
			'id' => 2,
			'name' => 'Vodkas',
			'description' => 'Text',
			'parent' => '0',
			'user' => 'vendor@vendor.com',
			'url' => 'Vodkas',
		));

		Categories::create(array(
			'id' => 3,
			'name' => 'Whisky',
			'description' => 'Text',
			'parent' => '0',
			'user' => 'vendor@vendor.com',
			'url' => 'Whisky',
		));

		Categories::create(array(
			'id' => 4,
			'name' => 'Tintos',
			'description' => 'Text',
			'parent' => 1,
			'user' => 'vendor@vendor.com',
			'url' => 'Tintos',
		));

		Categories::create(array(
			'id' => 5,
			'name' => 'Blanco',
			'description' => 'Text',
			'parent' => 2,
			'user' => 'vendor@vendor.com',
			'url' => 'Blanco',
		));

		Categories::create(array(
			'id' => 6,
			'name' => 'Malta',
			'description' => 'Text',
			'parent' => 3,
			'user' => 'vendor@vendor.com',
			'url' => 'Malta',
		));
	}

}