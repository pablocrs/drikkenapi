<?php

class PagesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('pages')->delete();

        Pages::create(array (
            "name"           => "Home",
            "title"           => "Inicio",
            "description"      => "home meta descripcion",
            "lang"         => "ES",
            "parent"        => "0",
            "user"        => "vendor@vendor.com",
            "url"        => "inicio"
        ));

        Pages::create(array (
            "name"           => "Home",
            "title"           => "Home",
            "description"      => "home meta description",
            "lang"         => "EN",
            "parent"        => "0",
            "user"        => "vendor@vendor.com",
            "url"        => "home"
        ));

        Pages::create(array (
            "name"           => "About",
            "title"           => "Nosotros",
            "description"      => "Descripcion meta description",
            "lang"         => "ES",
            "parent"        => "0",
            "user"        => "vendor@vendor.com",
            "url"        => "nosotros"
        ));

        Pages::create(array (
            "name"           => "Legal",
            "title"           => "Legal",
            "description"      => "Legal meta description",
            "lang"         => "ES",
            "parent"        => "0",
            "user"        => "vendor@vendor.com",
            "url"        => "legal"
        ));
    }

}