<?php

class ZonesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('zones')->delete();

		Zones::create(array(
			'name' => 'Malaga Centro',
			'user' => 'vendor@vendor.com',
			'description' => 'Seo text malaga',
			'url' => 'Malaga-Centro'
		));

		Zones::create(array(
			'name' => 'Benalmadena',
			'user' => 'vendor@vendor.com',
			'description' => 'Seo text lagunillas',
			'url' => 'Benalmadena'
		));

		Zones::create(array(
			'name' => 'Malaga Teatinos',
			'user' => 'vendor@vendor.com',
			'description' => 'Seo text teatinos',
			'url' => 'Málaga-Teatinos'
		));
	}
}

?>