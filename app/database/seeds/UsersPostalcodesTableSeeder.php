<?php

class UserPostalcodesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users_zones')->delete();

		UserPostalcodes::create(array(
			'user' => 'vendor@vendor.com',
			'postalcode' => '1',
		));

		UserPostalcodes::create(array(
			'user' => 'vendor@vendor.com',
			'postalcode' => '2',
		));

		UserPostalcodes::create(array(
			'user' => 'vendor@vendor.com',
			'postalcode' => '3',
		));
	}
}

?>