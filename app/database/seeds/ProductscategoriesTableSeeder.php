<?php

class ProductscategoriesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('products_categories')->delete();

		ProductCategories::create(array(
			'product' => '1',
			'category' => '1',
			'user' => 'vendor@vendor.com',
		));
		ProductCategories::create(array(
			'product' => '1',
			'category' => '4',
			'user' => 'vendor@vendor.com',
		));
		ProductCategories::create(array(
			'product' => '3',
			'category' => '2',
			'user' => 'vendor@vendor.com',
		));
		ProductCategories::create(array(
			'product' => '2',
			'category' => '3',
			'user' => 'vendor@vendor.com',
		));
		ProductCategories::create(array(
			'product' => '2',
			'category' => '6',
			'user' => 'vendor@vendor.com',
		));
	}

}