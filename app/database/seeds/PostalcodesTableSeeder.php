<?php

class PostalcodesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('postalcodes')->delete();

		Postalcodes::create(array(
			'number' => '29015',
			'user' => 'vendor@vendor.com',
			'zone' => 1,
		));

		Postalcodes::create(array(
			'number' => '29012',
			'user' => 'vendor@vendor.com',
			'zone' => 2,
		));

		Postalcodes::create(array(
			'number' => '29009',
			'user' => 'vendor@vendor.com',
			'zone' => 3,
		));
	}
}

?>