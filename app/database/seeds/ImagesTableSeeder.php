<?php

class ImagesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('images')->delete();

		Images::create(array(
			'id' => 1,
			'alt' => 'Vino tinto',
			'user' => 'vendor@vendor.com',
			'url' => '/dir/to/img',
		));
		Images::create(array(
			'alt' => 'Smirnof',
			'user' => 'vendor@vendor.com',
			'url' => '/dir/to/img',
		));
		Images::create(array(
			'alt' => 'Whisky',
			'user' => 'vendor@vendor.com',
			'url' => '/dir/to/img',
		));
		Images::create(array(
			'alt' => 'Coca cola',
			'user' => 'vendor@vendor.com',
			'url' => '/dir/to/img',
		));
	}

}