<?php


class TextsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('texts')->delete();

        Texts::create(array (
            "name"         => "BigTitle",
            "text"         => "Titulo principal",
            "page"         => "home",
            "user"         => "admin@admin.com",
            "lang"         => "ES"
        ));

        Texts::create(array (
            "name"         => "SmallTitle",
            "text"         => "Titulo secundario",
            "page"         => "home",
            "user"         => "admin@admin.com",
            "lang"         => "ES"
        ));

        Texts::create(array (
            "name"         => "InfoText",
            "text"         => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies arcu",
            "page"         => "about",
            "user"         => "vendor@vendor.com",
            "lang"         => "ES"
        ));

	}

}