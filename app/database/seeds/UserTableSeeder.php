<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create(array(
			'name' => 'Admin',
			'email' => 'admin@admin.com',
			'password' => Hash::make('admin'),
			'type' => 1,
			'zone' => 'Malaga',
			'postalcode' => '29009',
			'address' => 'C:/Carreteria nº2 1ºA',
			'phone' => '634587478',
			'image' => 'http://placekitten.com/50/50'
		));

		User::create(array(
			'name' => 'Vendor',
			'email' => 'vendor@vendor.com',
			'password' => Hash::make('vendor'),
			'type' => 2,
			'zone' => 'Malaga',
			'postalcode' => '29019',
			'address' => 'C:/Alora nº4 4ºD',
			'phone' => '63423757',
			'image' => 'http://placekitten.com/51/51'
		));

		User::create(array(
			'name' => 'User',
			'email' => 'user@user.com',
			'password' => Hash::make('user'),
			'type' => 3,
			'zone' => 'Malaga',
			'postalcode' => '29019',
			'address' => 'C:/Manrique nº8 6ºF',
			'phone' => '95325757',
			'image' => 'http://placekitten.com/52/52'
		));
	}
}

?>