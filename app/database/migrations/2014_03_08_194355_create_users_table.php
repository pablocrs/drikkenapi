<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name')->unique();

            $table->string('email')->unique();
            $table->string('password');
            $table->string('type');

            $table->string('zone');
            $table->string('postalcode');

            $table->string('address');
            $table->bigInteger('phone');

            $table->string('image');

            $table->string('token');

			$table->softDeletes();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}