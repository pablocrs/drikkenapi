<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location', function(Blueprint $table) {
			$table->increments('id');

			$table->string('ip');
			$table->string('isoCode');
			$table->string('country');
			$table->string('city');
			$table->string('state');
			$table->string('postal_code');
			$table->decimal('latitude');
			$table->decimal('longitude');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location');
	}

}
