<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('user');
			$table->bigInteger('phone');			
			$table->bigInteger('postalcode');			
			$table->string('address');			
			$table->string('zone');
			$table->string('comments');
			
			$table->string('payment');			
			$table->string('prepaid');			
			$table->string('delivery');
						
			$table->bigInteger('price');		
			$table->bigInteger('pvp');		

			$table->bigInteger('location');					

			$table->string('vendor');

			$table->boolean('active');

            $table->string('status');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
