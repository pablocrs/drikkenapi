<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::controller('api', 'ApiController');

//Route::controller('cart', 'CartController');

//Route::controller('cart', 'CartController');

Route::group(array('prefix' => 'service'), function() {
	Route::resource('auth', 'AuthController');
    Route::resource('files', 'FilesController');
});

Route::group(array('prefix' => 'api'), function() {
	Route::resource('products', 'ProductsController');
	Route::resource('productscategories', 'ProductsCategoriesController');
	Route::resource('productsimages', 'ProductsImagesController');
	Route::resource('categories', 'CategoriesController');

	Route::resource('zones', 'ZoneController');
	Route::resource('postalcodes', 'PostalcodesController');
	
	Route::resource('cart', 'CartController');
	Route::resource('orders', 'OrdersController');
    Route::resource('notifications', 'NotificationsController');

    Route::resource('files', 'FilesController');

    Route::resource('pages', 'PagesController');
});
